package com.daoImpl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.dao.UserDao;
import com.entities.Users;

@Repository
@Transactional
public class UsersDaoImpl implements UserDao {

	@Autowired
	SessionFactory session;
	
	@Override
	public List<Users> list() {
		return session.getCurrentSession().createQuery("from Users").list();
		
	}

	@Override
	public boolean delete(Users user) {
		try{
		session.getCurrentSession().delete(user);
		}
		catch (Exception e) {
			
			return false;
		}
		return true;
		
	}

	@Override
	public boolean saveOrUpdate(Users user) {
		session.getCurrentSession().saveOrUpdate(user);
		return true;
	}

}
